#pragma once
#ifndef TRAINS_ARRAY_CONTAINER_H
#define TRAINS_ARRAY_CONTAINER_H

#include "Train.hpp"

class TrainsArrayContainer {
    private:
	Train *array;
	int size;

    public:
	TrainsArrayContainer();
	TrainsArrayContainer(int size);
	TrainsArrayContainer(const TrainsArrayContainer &copy);
	~TrainsArrayContainer();

	Train &getArray();

	int getSize() const;

	void addElement(const Train &element, size_t position = 1);

	void removeElement(size_t index);

	Train &getElement(size_t index);

	/** Функція, для сортування поїздів
*/
	void sort_trains();

	/** Функція, для виводу массиву структур на екран
*/
	void print_trains();
};

#endif //TRAINS_ARRAY_CONTAINER_H
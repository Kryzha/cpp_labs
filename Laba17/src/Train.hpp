#pragma once
#ifndef TRAINS_H
#define TRAINS_H

#include <cstdio>
#include <string>
#include "TrainWay.hpp"

using namespace std;

/** Константа, яка відображає, скільки максимум символів може бути розташовано у файлі */
#define BUFER 10000

/** Types of trains */
enum TrainType { PUFFER, LOCOMOTIVE, ELECTRIC_LOCOMOTIVE };

class Train {
    private:
	bool need_to_fix;
	string train_number;
	int carriage_number;
	TrainWay trainway;
	enum TrainType train_type;

    public:
	Train();
	Train(bool fix, string train_number, int carriage_number, string start,
	      string stop, string train_type);
	Train(Train &copy);
	~Train();

	bool getFix();
	void setFix(bool fix);

	string getTrainNumber() const;
	void setTrainNumber(string train_number);

	int getCarriageNumber() const;
	void setCarriageNumber(int number);

	TrainWay &getTrainWay();
	void setTrainWay(string from, string to);

	TrainType getTrainType() const;
	void setTrainType(string type);

	void printTrain() const
	{
		this->trainway.printWay();
		cout << "Information: "
		     << "need to fix or not: " << this->need_to_fix << endl
		     << "Train number: " << this->train_number << endl
		     << "Carriage number: " << this->carriage_number << endl
		     << "Train type: " << this->train_type << endl;
	}
};

#endif //TRAINS_H

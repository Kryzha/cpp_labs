#include "TrainsArrayContainer.hpp"

TrainsArrayContainer::TrainsArrayContainer() : size(0)
{
	this->array = new Train[this->size];
}
TrainsArrayContainer::TrainsArrayContainer(int size) : size(size)
{
	this->array = new Train[this->size];
}
TrainsArrayContainer::TrainsArrayContainer(const TrainsArrayContainer &copy)
	: array(copy.array), size(copy.size)
{
}
TrainsArrayContainer::~TrainsArrayContainer()
{
	delete[] this->array;
}

Train &TrainsArrayContainer::getArray()
{
	return *TrainsArrayContainer::array;
}

int TrainsArrayContainer::getSize() const
{
	return this->size;
}

void TrainsArrayContainer::addElement(const Train &element, size_t position)
{
	auto *new_array = new Train[(TrainsArrayContainer::size + 1)];

start_checking_position:
	if (position > TrainsArrayContainer::size) {
		position = TrainsArrayContainer::size + 1;
		for (int i = 0; i < TrainsArrayContainer::size; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}
		new_array[position - 1] = element;
	} else if (position == 0) {
		position = 1;
		goto start_checking_position;
	} else {
		for (int i = 0; i < position - 1; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}

		new_array[position - 1] = element;

		for (int i = position; i < TrainsArrayContainer::size + 1;
		     i++) {
			new_array[i] = TrainsArrayContainer::array[i - 1];
		}
	}

	delete[] this->array;
	TrainsArrayContainer::array = new_array;
	TrainsArrayContainer::size++;
}

void TrainsArrayContainer::removeElement(size_t index)
{
	auto *new_array = new Train[TrainsArrayContainer::size - 1];

	if (index > TrainsArrayContainer::size - 1) {
		index = TrainsArrayContainer::size - 1;

		for (int i = 0; i < index; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}
	} else {
		for (int i = 0; i < index; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}

		for (int i = TrainsArrayContainer::size - 1; i > index; i--) {
			new_array[i - 1] = TrainsArrayContainer::array[i];
		}
	}

	delete[] this->array;
	TrainsArrayContainer::array = new_array;
	TrainsArrayContainer::size--;
}

Train &TrainsArrayContainer::getElement(size_t index)
{
	if (index > TrainsArrayContainer::size - 1) {
		index = TrainsArrayContainer::size - 1;
	}

	return TrainsArrayContainer::array[index];
}

/** Функція, для сортування поїздів */
void TrainsArrayContainer::sort_trains()
{
	for (int i = 0; i < this->size; i++) {
		if (this->array[i].getCarriageNumber() <= 10 &&
		    !this->array[i].getFix()) {
			this->removeElement(i);
			i--;
		}
	}
}

/** Функція, для виводу массиву структур на екран */
void TrainsArrayContainer::print_trains()
{
	cout << "Array size: " << TrainsArrayContainer::size << endl;
	for (int i = 0; i < TrainsArrayContainer::size; i++) {
		cout << (i + 1) << "." << endl;
		TrainsArrayContainer::array[i].printTrain();
	}
}

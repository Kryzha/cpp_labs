#include "TrainWay.hpp"

TrainWay::TrainWay() : start("Start"), stop("Stop")
{
}

TrainWay::TrainWay(string start, string stop) : start(start), stop(stop)
{
}

TrainWay::TrainWay(TrainWay &copy) : start(copy.start), stop(copy.stop)
{
}

TrainWay::~TrainWay() = default;

string *TrainWay::getStart()
{
	return &this->start;
}
void TrainWay::setStart(string start)
{
	this->start = start;
}

string *TrainWay::getStop()
{
	return &this->stop;
}
void TrainWay::setStop(string stop)
{
	this->stop = stop;
}

void TrainWay::printWay() const
{
	cout << "Start: " << this->start << ", end: " << this->stop << endl;
}

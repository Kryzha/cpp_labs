#include "Train.hpp"
#include "TrainsArrayContainer.hpp"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	/* construct in work (default construct is without arguments) */
	TrainsArrayContainer trains_array_container_1(3);

	/* seeding array */
	trains_array_container_1.getElement(0).getTrainWay().setStart(
		"Nikopol");
	trains_array_container_1.getElement(0).getTrainWay().setStop("Kharkiv");
	trains_array_container_1.getElement(0).setTrainNumber("Ukraine");
	trains_array_container_1.getElement(1).setCarriageNumber(228337);
	trains_array_container_1.getElement(2).setTrainType("LOCOMOTIVE");
	trains_array_container_1.getElement(2).setTrainNumber("Single");
	trains_array_container_1.getElement(2).setFix(true);

	/* printing trains */
	trains_array_container_1.print_trains();

	trains_array_container_1.sort_trains();

	cout << endl << "After filtration: " << endl << endl;

	trains_array_container_1.print_trains();

	return 0;
}

#pragma once
#ifndef LABA17_TRAINWAY_HPP
#define LABA17_TRAINWAY_HPP

#include <iostream>
#include <string>

using namespace std;

class TrainWay {
    private:
	string start;
	string stop;

    public:
	TrainWay();
	TrainWay(string start, string stop);
	TrainWay(TrainWay &copy);
	~TrainWay();

	string *getStart();
	void setStart(string start);

	string *getStop();
	void setStop(string stop);

	void printWay() const;
};

#endif //LABA17_TRAINWAY_HPP

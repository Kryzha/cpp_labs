# Лабораторна робота №17. ООП. Вступ до ООП

## Мета

Отримати навички розробки програм мовою С++, які написані за принципом ООП.

## 1 Вимоги

### 1.1 Розробник

Інформація

- Крижановський Ілля Миколайович;
- КН-921Б;

### 1.2 Загальне завдання

- Переробити лабораторну роботу номер 15 під ООП стиль.
- Зробити так, щоб модульні тести виконувались за допомогою гугл тестів

### 1.3 Задача
- Переробити структури у класи
- Зробити конструктори
- Зробити роботи згідно вимогам
- Переробити `Makefile` під гугл тести


> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin`.
>
> Зробити документацію до проекта можна за допомогую команди `make docgen`.
>
> Зробити модульні тести до ціеї програми можна за допомогою команди `make test`.

В результаті виконання лабораторної роботи були розроблені класси, в яких є 3 типа конструкторів:

- За замовчуванням (Виконується при створенні обьєкта, якщо данні не були передані);
- Звичайний конструктор (Виконується, якщо при створенні були передані якійсь данні);
- Копіюючий конструктор (Виконується, коли треба скопіювати обьєкт);
- Також є деструктор (Виконується при видаленні обьєкта);

Приклад ціх конструкторів зображено нижче:

```cpp
//entity.h
TrainsArrayContainer();
	TrainsArrayContainer(int size);
	TrainsArrayContainer(const TrainsArrayContainer &copy);
	~TrainsArrayContainer();
//entity.cpp
TrainsArrayContainer::TrainsArrayContainer() : size(0)
{
	this->array = new Train[this->size];
}
TrainsArrayContainer::TrainsArrayContainer(int size) : size(size)
{
	this->array = new Train[this->size];
}
TrainsArrayContainer::TrainsArrayContainer(const TrainsArrayContainer &copy)
	: array(copy.array), size(copy.size)
{
}
TrainsArrayContainer::~TrainsArrayContainer() = default;
```

Корректний результат виконання программи:

```ABAP
./dist/main.bin
Array size: 3
1.
Start: Nikopol, end: Kharkiv
Information: need to fix or not: 0
Train number: Ukraine
Carriage number: 10
Train type: 0
2.
Start: Start, end: Stop
Information: need to fix or not: 0
Train number: Train
Carriage number: 228337
Train type: 0
3.
Start: Start, end: Stop
Information: need to fix or not: 1
Train number: Single
Carriage number: 10
Train type: 1

After filtration: 

Array size: 2
1.
Start: Start, end: Stop
Information: need to fix or not: 0
Train number: Train
Carriage number: 228337
Train type: 0
2.
Start: Start, end: Stop
Information: need to fix or not: 1
Train number: Single
Carriage number: 10
Train type: 1

```

## Висновок

На цій лабораторній работі я навчився робити програми за принципом ООП стилю C++.
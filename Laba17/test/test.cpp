#include "Train.hpp"
#include "TrainsArrayContainer.hpp"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    /* construct in work (default construct is without arguments) */
    TrainsArrayContainer trains_array_container_1(3);

    /* seeding array */
    trains_array_container_1.getElement(0).getTrainWay().setStart(
            "Nikopol");
    trains_array_container_1.getElement(0).getTrainWay().setStart(
            "Kharkiv");
    trains_array_container_1.getElement(0).setTrainNumber("Ukraine");
    trains_array_container_1.getElement(1).setCarriageNumber(228337);
    trains_array_container_1.getElement(2).setTrainType("LOCOMOTIVE");
    trains_array_container_1.getElement(2).setTrainNumber("Single");

    trains_array_container_1.sort_trains();

    ASSERT_EQ(1, trains_array_container_1.getSize());
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#include "Train.hpp"
#include "TrainsArrayContainer.hpp"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	string input_file = "assets/input.txt";
	string output_file = "assets/output.txt";

	/* construct in work (default construct is without arguments) */
	TrainsArrayContainer trains_array_container_1("3");

	/* seeding array */
	trains_array_container_1.getElement(0).getTrainWay().setStart(
		"Nikopol");
	trains_array_container_1.getElement(0).getTrainWay().setStop("Kharkiv");
	trains_array_container_1.getElement(0).setTrainNumber("Ukraine");
	trains_array_container_1.getElement(1).setCarriageNumber(228337);
	trains_array_container_1.getElement(2).setTrainType("LOCOMOTIVE");
	trains_array_container_1.getElement(2).setTrainNumber("Single");
	trains_array_container_1.getElement(2).setFix(true);

	/* printing trains */
	cout << trains_array_container_1.containerToString();

	trains_array_container_1.sort_trains();

	cout << endl << "After filtration: " << endl << endl;

	cout << trains_array_container_1.containerToString();

	cout << endl << "After reading from file: " << endl << endl;

	trains_array_container_1.readFromFile(input_file);

	cout << trains_array_container_1.containerToString();

	trains_array_container_1.sort_trains();

	cout << endl << "After reading from file AND SORTING: " << endl << endl;

	cout << trains_array_container_1.containerToString();

	trains_array_container_1.writeToFile(output_file);

	return 0;
}

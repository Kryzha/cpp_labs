#pragma once
#ifndef TRAINS_H
#define TRAINS_H

#include <cstdio>
#include <istream>
#include <ostream>
#include "TrainWay.hpp"

using std::istream;
using std::ostream;

/** Константа, яка відображає, скільки максимум символів може бути розташовано у файлі */
#define BUFER 10000

/** Types of trains */
enum TrainType { PUFFER, LOCOMOTIVE, ELECTRIC_LOCOMOTIVE };

class Train {
    private:
	bool need_to_fix;
	string train_number;
	int carriage_number;
	TrainWay trainway;
	enum TrainType train_type;
	string train_string;

    public:
	Train();
	Train(string input_string);
	Train(Train &copy);
	~Train();

	bool getFix();
	void setFix(bool fix);

	string getTrainNumber() const;
	void setTrainNumber(string train_number);

	int getCarriageNumber() const;
	void setCarriageNumber(int number);

	TrainWay &getTrainWay();
	void setTrainWay(string from, string to);

	TrainType getTrainType() const;
	void setTrainType(string type);

	string &trainToString();

	Train &operator=(const Train &obj);

	bool operator==(const Train &obj) const;

	bool operator!=(const Train &obj) const;

	friend ostream &operator<<(ostream &output, const Train &object);

	friend istream &operator>>(istream &input_stream, Train &object);
};

#endif //TRAINS_H

#pragma once
#ifndef TRAINS_ARRAY_CONTAINER_H
#define TRAINS_ARRAY_CONTAINER_H

#include <fstream>
#include "Train.hpp"

using std::cerr;
using std::cout;
using std::ifstream;
using std::ofstream;

class TrainsArrayContainer {
    private:
	Train *array;
	int size;
	string container_to_string;

    public:
	TrainsArrayContainer();
	TrainsArrayContainer(string input_string);
	TrainsArrayContainer(const TrainsArrayContainer &copy);
	~TrainsArrayContainer();

	Train &getArray();

	int getSize() const;

	void addElement(const Train &element, size_t position = 1);

	void removeElement(size_t index);

	Train &getElement(size_t index);

	/** Функція, для сортування поїздів */
	void sort_trains();

	/** Функція, для виводу массиву структур на екран */
	string &containerToString();

	void readFromFile(string &file_name);

	void writeToFile(string &file_name);

	Train &operator[](size_t i) const;

	friend ostream &operator<<(ostream &output,
				   const TrainsArrayContainer &object);

	friend istream &operator>>(istream &input_stream,
				   TrainsArrayContainer &object);
};

#endif //TRAINS_ARRAY_CONTAINER_H
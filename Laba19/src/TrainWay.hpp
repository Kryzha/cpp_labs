#pragma once
#ifndef LABA17_TRAINWAY_HPP
#define LABA17_TRAINWAY_HPP

#include <iostream>
#include <string>
#include <sstream>

using std::endl;
using std::string;
using std::stringstream;

class TrainWay {
    private:
	string start;
	string stop;
	string train_way_string;

    public:
	TrainWay();
	TrainWay(string input_string);
	TrainWay(TrainWay &copy);
	~TrainWay();

	string getStart() const;
	void setStart(string start);

	string getStop() const;
	void setStop(string stop);

	string &trainWayToString();
};

#endif //LABA17_TRAINWAY_HPP

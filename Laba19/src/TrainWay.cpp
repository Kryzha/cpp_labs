#include "TrainWay.hpp"

TrainWay::TrainWay() : start("Start"), stop("Stop")
{
}

TrainWay::TrainWay(string input_string)
{
	stringstream input(input_string);

	input >> this->start >> this->stop;
}

TrainWay::TrainWay(TrainWay &copy) : start(copy.start), stop(copy.stop)
{
}

TrainWay::~TrainWay() = default;

string TrainWay::getStart() const
{
	return this->start;
}
void TrainWay::setStart(string start)
{
	this->start = start;
}

string TrainWay::getStop() const
{
	return this->stop;
}
void TrainWay::setStop(string stop)
{
	this->stop = stop;
}

string &TrainWay::trainWayToString()
{
	stringstream output;
	output << "Start: " << this->start << ", end: " << this->stop << endl;
	this->train_way_string = output.str();
	return this->train_way_string;
}

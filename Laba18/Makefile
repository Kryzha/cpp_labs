CC := clang
SRC_EXTENSION := .cpp
MAIN_EXE := main.bin
TEST_EXE := test.bin
INPUT_FILES := input.txt output.txt
EXE_DIR := dist
MAIN_SRC_DIR := src
TEST_SRC_DIR := test
ASSETS_SRC_DIR := assets
MAIN_PATH := $(EXE_DIR)/$(MAIN_EXE)
TEST_PATH := $(EXE_DIR)/$(TEST_EXE)
INPUT_PATH := $(ASSETS_SRC_DIR)/$(INPUT_FILES)
LIB_OBJS  := TrainWay.o Train.o TrainsArrayContainer.o
MAIN_OBJS := main.o
TEST_OBJS := test.o
CFLAGS    := -Wall -I h -I -g -g0 -fprofile-instr-generate -fcoverage-mapping
CLIB := -lstdc++
LDFLAGS   := -fprofile-instr-generate -fcoverage-mapping
TESTLIB   := -lm -I h -lgtest -lrt -pthread -lsubunit
V_FLAGS := --tool=memcheck --leak-check=full --show-reachable=yes \
	--undef-value-errors=yes --track-origins=no --child-silent-after-fork=no \
	--trace-children=no --error-exitcode=1

.PHONY: all distclean clean leak-check

all: distclean format $(MAIN_PATH) $(TEST_PATH)
	./$(EXE_DIR)/$(MAIN_EXE)

leak-check: distclean format $(MAIN_PATH) $(TEST_PATH)
	valgrind $(V_FLAGS) --log-file=$(EXE_DIR)/valgrind.log --xml-file=$(EXE_DIR)/valgrind.xml --xml=yes ./$(EXE_DIR)/$(MAIN_EXE)
	valgrind $(V_FLAGS) --log-file=$(EXE_DIR)/valgrind.log --xml-file=$(EXE_DIR)/valgrind.xml --xml=yes ./$(EXE_DIR)/$(TEST_EXE)

$(MAIN_PATH): $(LIB_OBJS) $(MAIN_OBJS) | $(EXE_DIR)
	$(CC) $(LDFLAGS) $^ $(CLIB) -o $@

$(TEST_PATH): $(LIB_OBJS) $(TEST_OBJS) | $(EXE_DIR)
	$(CC) $(LDFLAGS) $^ $(CLIB) $(TESTLIB) -o $@

%.o: $(MAIN_SRC_DIR)/%$(SRC_EXTENSION)
	$(CC) -c $(CFLAGS) $< -o $@
	
%.o: $(TEST_SRC_DIR)/%$(SRC_EXTENSION)
	$(CC) -c $(CFLAGS) -I$(MAIN_SRC_DIR) $< -o $@
	
$(EXE_DIR):
	mkdir -p $@

test: $(TEST_PATH)
	./$(EXE_DIR)/$(TEST_EXE)
	llvm-profdata merge -sparse default.profraw -o default.profdata
	llvm-cov report $(TEST_PATH) -instr-profile=default.profdata $(MAIN_SRC_DIR)/*$(SRC_EXTENSION)
	llvm-cov show $(TEST_PATH) -instr-profile=default.profdata $(MAIN_SRC_DIR)/*$(SRC_EXTENSION) -format html > coverage.html

distclean: clean
	@$(RM) -rv $(EXE_DIR) html coverage.html

clean:
	@$(RM) -v $(LIB_OBJS) $(MAIN_OBJS) $(TEST_OBJS) *.profdata *.profraw *.info

format: $(MAIN_SRC_DIR)/.clang-format
	clang-format $(MAIN_SRC_DIR)/* -i

docgen: Doxyfile
	doxygen Doxyfile
	xdg-open $(EXE_DIR)/html/index.html
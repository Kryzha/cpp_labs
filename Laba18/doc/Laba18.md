# Laboratory work №18. OOP. FLOWS

## Target

Gain skills in developing C ++ programs that have input / output functions and file usage..

## 1 Requirements

### 1.1 Developer

Information

- Kryzhanovskyi Illia Mykolayovich;
- group КН-921B;

### 1.2 Actual task

- Rebuild laboratory work number 17 (delete C language functions).
- Add work with files (In C++ language type)
- Remake output functions, so they actualy not printing anything. They must convert data to string using stringstream
- Not tu use `using namespace std`, instead of it use sentences like `using std::string`

### 1.3 Task solving

- Retype all functions to C++ style
- Add file works functions to DynamicArrayContainerClass

> To start compile the program use `make` bush command. Than to start it use command below in the terminal:
>
> `./dist/main.bin`.
>
> To make a documentation use: `make docgen`.
>
> To make a module tests use: `make test`.

The result of execured program:

- Result of executed file in `dist/output.txt`

```shell
./dist/main.bin
Array size: 3
1.
Start: Nikopol, end: Kharkiv
Information: need to fix or not: 0
Train number: Ukraine
Carriage number: 10
Train type: 0
2.
Start: Start, end: Stop
Information: need to fix or not: 0
Train number: Train
Carriage number: 228337
Train type: 0
3.
Start: Start, end: Stop
Information: need to fix or not: 1
Train number: Single
Carriage number: 10
Train type: 1

After filtration: 

Array size: 2
1.
Start: Start, end: Stop
Information: need to fix or not: 0
Train number: Train
Carriage number: 228337
Train type: 0
2.
Start: Start, end: Stop
Information: need to fix or not: 1
Train number: Single
Carriage number: 10
Train type: 1

After reading from file: 

Array size: 3
1.
Start: Niotkuda, end: Nikuda
Information: need to fix or not: 0
Train number: Illia
Carriage number: 8
Train type: 1
2.
Start: Vika, end: Anya
Information: need to fix or not: 0
Train number: Yegor
Carriage number: 9
Train type: 1
3.
Start: Masha, end: Alla
Information: need to fix or not: 1
Train number: Zheka
Carriage number: 100
Train type: 1

After reading from file AND SORTING: 

Array size: 1
1.
Start: Masha, end: Alla
Information: need to fix or not: 1
Train number: Zheka
Carriage number: 100
Train type: 1
```



## Conclusion

In this laboratory work i learned how to work with flows in C++ language.
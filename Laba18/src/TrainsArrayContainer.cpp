#include "TrainsArrayContainer.hpp"

TrainsArrayContainer::TrainsArrayContainer() : size(0)
{
	this->array = new Train[this->size];
}
TrainsArrayContainer::TrainsArrayContainer(string input_string)
{
	stringstream input(input_string);
	input >> this->size;
	this->array = new Train[this->size];
}
TrainsArrayContainer::TrainsArrayContainer(const TrainsArrayContainer &copy)
	: array(copy.array), size(copy.size)
{
}
TrainsArrayContainer::~TrainsArrayContainer()
{
	delete[] this->array;
}

Train &TrainsArrayContainer::getArray()
{
	return *TrainsArrayContainer::array;
}

int TrainsArrayContainer::getSize() const
{
	return this->size;
}

void TrainsArrayContainer::addElement(const Train &element, size_t position)
{
	auto *new_array = new Train[(TrainsArrayContainer::size + 1)];

start_checking_position:
	if (position > TrainsArrayContainer::size) {
		position = TrainsArrayContainer::size + 1;
		for (int i = 0; i < TrainsArrayContainer::size; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}
		new_array[position - 1] = element;
	} else if (position == 0) {
		position = 1;
		goto start_checking_position;
	} else {
		for (int i = 0; i < position - 1; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}

		new_array[position - 1] = element;

		for (int i = position; i < TrainsArrayContainer::size + 1;
		     i++) {
			new_array[i] = TrainsArrayContainer::array[i - 1];
		}
	}

	delete[] this->array;
	TrainsArrayContainer::array = new_array;
	TrainsArrayContainer::size++;
}

void TrainsArrayContainer::removeElement(size_t index)
{
	auto *new_array = new Train[TrainsArrayContainer::size - 1];

	if (index > TrainsArrayContainer::size - 1) {
		index = TrainsArrayContainer::size - 1;

		for (int i = 0; i < index; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}
	} else {
		for (int i = 0; i < index; i++) {
			new_array[i] = TrainsArrayContainer::array[i];
		}

		for (int i = TrainsArrayContainer::size - 1; i > index; i--) {
			new_array[i - 1] = TrainsArrayContainer::array[i];
		}
	}

	delete[] this->array;
	TrainsArrayContainer::array = new_array;
	TrainsArrayContainer::size--;
}

Train &TrainsArrayContainer::getElement(size_t index)
{
	if (index > TrainsArrayContainer::size - 1) {
		index = TrainsArrayContainer::size - 1;
	}

	return TrainsArrayContainer::array[index];
}

/** Функція, для сортування поїздів */
void TrainsArrayContainer::sort_trains()
{
	for (int i = 0; i < this->size; i++) {
		if (this->array[i].getCarriageNumber() <= 10 &&
		    !this->array[i].getFix()) {
			this->removeElement(i);
			i--;
		}
	}
}

/** Функція, для виводу массиву структур на екран */
string &TrainsArrayContainer::containerToString()
{
	stringstream output;

	output << "Array size: " << TrainsArrayContainer::size << endl;
	for (int i = 0; i < TrainsArrayContainer::size; i++) {
		output << (i + 1) << "." << endl;
		output << TrainsArrayContainer::array[i].trainToString();
	}

	this->container_to_string = output.str();

	return this->container_to_string;
}

void TrainsArrayContainer::readFromFile(string &file_name)
{
	delete[] this->array;
	this->size = 0;
	this->array = new Train[this->size];

	ifstream indata;
	string data;

	indata.open(file_name);
	if (!indata) {
		cerr << "Error file could not be opened" << endl;
		exit(1);
	}

	while (!indata.eof()) {
		getline(indata, data);
		Train item(data);
		this->addElement(item);
	}
	indata.close();
}

void TrainsArrayContainer::writeToFile(string &file_name)
{
	ofstream out_data;

	out_data.open(file_name);
	for (int i = 0; i < this->size; i++) {
		out_data << this->array[i].getFix() << " "
			 << this->array[i].getTrainNumber() << " "
			 << this->array[i].getCarriageNumber() << " "
			 << this->array[i].getTrainWay().getStart() << " "
			 << this->array[i].getTrainWay().getStop() << " "
			 << this->array[i].getTrainType() << endl;
	}
	out_data.close();
}

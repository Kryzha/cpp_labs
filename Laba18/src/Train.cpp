#include "Train.hpp"

/* Init of TrainWay class methods */
Train::Train()
	: need_to_fix(0), train_number("Train"), carriage_number(10),
	  trainway(), train_type(PUFFER)
{
}

Train::Train(string input_string)
{
	stringstream input(input_string);
	string train_type, start, stop;
	int need_to_fix_int;

	input >> need_to_fix_int >> this->train_number >>
		this->carriage_number >> start >> stop >> train_type;

	if (need_to_fix_int == 0 || need_to_fix_int == 1) {
		this->need_to_fix = need_to_fix_int;
	} else {
		this->need_to_fix = 0;
	}

	this->trainway.setStart(start);
	this->trainway.setStop(stop);

	if (train_type == "PUFFER") {
		train_type = PUFFER;
	} else if (train_type == "LOCOMOTIVE") {
		train_type = LOCOMOTIVE;
	} else if (train_type == "ELECTRIC_LOCOMOTIVE") {
		train_type = ELECTRIC_LOCOMOTIVE;
	} else {
		train_type = PUFFER;
	}
}
Train::Train(Train &copy)
	: need_to_fix(copy.need_to_fix), train_number(copy.train_number),
	  carriage_number(copy.carriage_number), trainway(copy.trainway),
	  train_type(copy.train_type)
{
}

Train::~Train() = default;

bool Train::getFix()
{
	return this->need_to_fix;
}
void Train::setFix(bool fix)
{
	this->need_to_fix = fix;
}

string Train::getTrainNumber() const
{
	return this->train_number;
}
void Train::setTrainNumber(string train_number)
{
	this->train_number = train_number;
}

int Train::getCarriageNumber() const
{
	return this->carriage_number;
}
void Train::setCarriageNumber(int number)
{
	this->carriage_number = number;
}

TrainWay &Train::getTrainWay()
{
	return this->trainway;
}
void Train::setTrainWay(string from, string to)
{
	this->trainway.setStart(from);
	this->trainway.setStop(to);
}

TrainType Train::getTrainType() const
{
	return this->train_type;
}
void Train::setTrainType(string type)
{
	if (type == "PUFFER") {
		this->train_type = PUFFER;
	} else if (type == "LOCOMOTIVE") {
		this->train_type = LOCOMOTIVE;
	} else if (type == "ELECTRIC_LOCOMOTIVE") {
		this->train_type = ELECTRIC_LOCOMOTIVE;
	} else {
		this->train_type = PUFFER;
	}
}

string &Train::trainToString()
{
	stringstream output;
	output << this->trainway.trainWayToString() << "Information: "
	       << "need to fix or not: " << this->need_to_fix << endl
	       << "Train number: " << this->train_number << endl
	       << "Carriage number: " << this->carriage_number << endl
	       << "Train type: " << this->train_type << endl;

	this->train_string = output.str();
	return this->train_string;
}
# Laboratory work №21. OOP. Template functions and classes

## Target

Gain skills in developing C ++ programs that have dynamic arrays created via classes.

## 1 Requirements

### 1.1 Developer

Information

- Kryzhanovskyi Illia Mykolayovich;
- group КН-921B;

### 1.2 Task

- Created dynamic array for any type of data, using classes;

### 1.3 Actual task

• Create a template class-list (based on a dynamic array) that has a template field array (for any existing data type) • Create the following methods:

- output the contents of the array to the screen;
- determine the index of the transmitted element in a given array;
- sort array elements;
- determine the value of the minimum element of the array;
- add an element to the end of the array;
- delete an element from the array by index.

### 1.4 Result

> To start compile the program use `make` bush command. Than to start it use command below in the terminal:
>
> `./dist/main.bin`.
>
> To make a documentation use: `make docgen`.
>
> To make a module tests use: `make test`.

The result of executed program:

```shell
./dist/main.bin
0. 5
1. 4
2. 2
3. 1
Min element: 1

Another array:

0. World
1. Hello
2. !
3.  
Min element:  
```



## Conclusion

In this laboratory work i learned how to create dynamic array classes for any type of data in C++ language.

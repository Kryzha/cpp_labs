#pragma once
#ifndef TRAINS_ARRAY_CONTAINER_H
#define TRAINS_ARRAY_CONTAINER_H

#include <iostream>

using std::cout;
using std::endl;
using std::string;

template <typename T> class DynamicArray {
    private:
	T *array;
	int size;

    public:
	DynamicArray();
	DynamicArray(int size);
	DynamicArray(const DynamicArray &copy);
	~DynamicArray();

	T getArray() const;

	int getSize() const;

	void addElement(const T &element, size_t position = 1);

	void removeElement(size_t index);

	T getElement(size_t index) const;

	void setElement(size_t index, T value);

	T getMinElement();

	int getIndex(T value) const;

	/** Функція, для сортування поїздів */
	void sort(string way = "DESC");

	/** Функція, для виводу массиву структур на екран */
	void print() const;
};

/* init  of functions */
template <typename T> DynamicArray<T>::DynamicArray() : size(0)
{
	this->array = new T[this->size];
}
template <typename T> DynamicArray<T>::DynamicArray(int size)
{
	this->size = size;
	this->array = new T[this->size];
}
template <typename T>
DynamicArray<T>::DynamicArray(const DynamicArray &copy)
	: array(copy.array), size(copy.size)
{
}
template <typename T> DynamicArray<T>::~DynamicArray()
{
	delete[] this->array;
}

template <typename T> T DynamicArray<T>::getArray() const
{
	return DynamicArray::array;
}

template <typename T> int DynamicArray<T>::getSize() const
{
	return this->size;
}

template <typename T>
void DynamicArray<T>::addElement(const T &element, size_t position)
{
	auto *new_array = new T[(DynamicArray::size + 1)];

start_checking_position:
	if (position > DynamicArray::size) {
		position = DynamicArray::size + 1;
		for (int i = 0; i < DynamicArray::size; i++) {
			new_array[i] = DynamicArray::array[i];
		}
		new_array[position - 1] = element;
	} else if (position == 0) {
		position = 1;
		goto start_checking_position;
	} else {
		for (int i = 0; i < position - 1; i++) {
			new_array[i] = DynamicArray::array[i];
		}

		new_array[position - 1] = element;

		for (int i = position; i < DynamicArray::size + 1; i++) {
			new_array[i] = DynamicArray::array[i - 1];
		}
	}

	delete[] this->array;
	DynamicArray::array = new_array;
	DynamicArray::size++;
}

template <typename T> void DynamicArray<T>::removeElement(size_t index)
{
	auto *new_array = new T[DynamicArray::size - 1];

	if (index > DynamicArray::size - 1) {
		index = DynamicArray::size - 1;

		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArray::array[i];
		}
	} else {
		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArray::array[i];
		}

		for (int i = DynamicArray::size - 1; i > index; i--) {
			new_array[i - 1] = DynamicArray::array[i];
		}
	}

	delete[] this->array;
	DynamicArray::array = new_array;
	DynamicArray::size--;
}

template <typename T> T DynamicArray<T>::getElement(size_t index) const
{
	if (index > DynamicArray::size - 1) {
		index = DynamicArray::size - 1;
	}

	return DynamicArray::array[index];
}

template <typename T> void DynamicArray<T>::setElement(size_t index, T value)
{
	this->array[index] = value;
}

template <typename T> T DynamicArray<T>::getMinElement()
{
	T min_element = this->array[0];

	for (int i = 0; i < this->size; i++) {
		if (min_element > this->array[i]) {
			min_element = this->array[i];
		}
	}

	return min_element;
}

template <typename T> int DynamicArray<T>::getIndex(T value) const
{
	int index = -1;

	for (int i = 0; i < this->size; i++) {
		if (this->array[i] == value) {
			index = i;
		}
	}

	if (index == -1) {
		cout << "No element found, -1 returned" << endl;
	}

	return index;
}

/** Функція, для сортування поїздів */
template <typename T> void DynamicArray<T>::sort(string way)
{
	if (way == "ASC") {
		for (int i = 0; i < this->size; i++) {
			for (int j = this->size - 1; j > i; j--) {
				if (this->array[j - 1] < this->array[j]) {
					T x = array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else if (way == "DESC") {
		for (int i = 0; i < this->size; i++) {
			for (int j = this->size - 1; j > i; j--) {
				if (this->array[j - 1] > this->array[j]) {
					T x = this->array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else {
		cout << "Please, type correct way of sorting!" << endl;
	}
}

/** Функція, для виводу массиву структур на екран */
template <typename T> void DynamicArray<T>::print() const
{
	for (int i = 0; i < this->size; i++) {
		cout << i << ". " << this->array[i] << endl;
	}
}

#endif //TRAINS_ARRAY_CONTAINER_H
#include "DynamicArray.hpp"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	DynamicArray<int> array_1;
	array_1.addElement(5);
	array_1.addElement(4);
	array_1.addElement(2);
	array_1.addElement(1);
	array_1.sort("ASC");
	array_1.print();
	cout << "Min element: " << array_1.getMinElement() << endl;

	cout << endl << "Another array:" << endl << endl;

	DynamicArray<string> array_2;
	array_2.addElement("Hello");
	array_2.addElement(" ");
	array_2.addElement("World");
	array_2.addElement("!");
	array_2.sort("ASC");
	array_2.print();
	cout << "Min element: " << array_2.getMinElement() << endl;

	return 0;
}

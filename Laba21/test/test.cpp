#include "DynamicArray.hpp"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    int expected = 1;
    DynamicArray<int> array_1;
    array_1.addElement(5);
    array_1.addElement(4);
    array_1.addElement(2);
    array_1.addElement(1);

    int min_element = array_1.getMinElement();

    ASSERT_EQ(expected, min_element);
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

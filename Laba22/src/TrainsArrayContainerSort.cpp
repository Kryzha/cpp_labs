#include "TrainsArrayContainerSort.hpp"

void ArrayContainerSort::operator()(vector<string> *array)
{
	sort(array->begin(), array->end());
}
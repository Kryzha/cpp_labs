#include "TrainsArrayContainer.hpp"

ArrayContainer::ArrayContainer() : array()
{
}
ArrayContainer::ArrayContainer(const ArrayContainer &copy)
{
	for (int i = 0; i < this->array.size(); i++) {
		this->array.push_back(copy.array[i]);
	}
}
ArrayContainer::~ArrayContainer() = default;

int ArrayContainer::getSize() const
{
	return this->array.size();
}

void ArrayContainer::addElement(const string &element, size_t position)
{
	this->array.insert(this->array.begin() + position - 1, element);
}

void ArrayContainer::removeElement(size_t index)
{
	this->array.erase(this->array.begin() + index);
}

string ArrayContainer::getElement(size_t index) const
{
	if (index > this->array.size() - 1) {
		index = this->array.size() - 1;
	}

	return this->array[index];
}

/** Функція, для виводу массиву структур на екран */
void ArrayContainer::print() const
{
	cout << "Array size: " << this->array.size() << endl;
	for (int i = 0; i < this->array.size(); i++) {
		cout << (i + 1) << ". " << this->array[i] << endl;
	}
}

void ArrayContainer::sort_array()
{
	this->sort(&this->array);
}
#pragma once
#ifndef TRAINS_ARRAY_CONTAINER_H
#define TRAINS_ARRAY_CONTAINER_H

#include "TrainsArrayContainerSort.hpp"

using std::cout;
using std::endl;

class ArrayContainer {
    private:
	vector<string> array;
	ArrayContainerSort sort;

    public:
	ArrayContainer();
	ArrayContainer(const ArrayContainer &copy);
	~ArrayContainer();

	int getSize() const;

	void addElement(const string &element, size_t position = 1);

	void removeElement(size_t index);

	string getElement(size_t index) const;

	void sort_array();

	/** Функція, для виводу массиву структур на екран */
	void print() const;
};

#endif //TRAINS_ARRAY_CONTAINER_H
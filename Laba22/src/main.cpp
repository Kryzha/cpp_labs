#include "TrainsArrayContainer.hpp"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	ArrayContainer array_1;

	array_1.addElement("Hello");
	array_1.addElement(" ");
	array_1.addElement("World");
	array_1.addElement("!");
	array_1.sort_array();
	array_1.print();

	return 0;
}

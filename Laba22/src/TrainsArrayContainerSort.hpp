#pragma once
#ifndef LABA19_ARRAYCONTAINERSORT_H
#define LABA19_ARRAYCONTAINERSORT_H

#include <vector>
#include <string>
#include <bits/stdc++.h>

using std::sort;
using std::string;
using std::vector;

class ArrayContainerSort {
    public:
	void operator()(vector<string> *array);
};

#endif //LABA19_ARRAYCONTAINERSORT_H

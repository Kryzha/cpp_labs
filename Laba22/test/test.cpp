#include "TrainsArrayContainer.hpp"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    int expected = 4;
    ArrayContainer array_1;

    array_1.addElement("Hello");
    array_1.addElement(" ");
    array_1.addElement("World");
    array_1.addElement("!");

    ASSERT_EQ(expected, array_1.getSize());
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
